﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchingProgram : MonoBehaviour 
{
	public string password = "1234";
	public string input;

	public Sprite[] spriteArray;

	SpriteRenderer digit1;
	SpriteRenderer digit2;
	SpriteRenderer digit3;
	SpriteRenderer digit4;

	Renderer backgroundColour;

	// Use this for initialization
	void Start () 
	{
		backgroundColour = GetComponentInChildren<Renderer> ();

		digit1 = GameObject.FindGameObjectWithTag ("Digit1").GetComponent<SpriteRenderer> ();
		digit2 = GameObject.FindGameObjectWithTag ("Digit2").GetComponent<SpriteRenderer> ();
		digit3 = GameObject.FindGameObjectWithTag ("Digit3").GetComponent<SpriteRenderer> ();
		digit4 = GameObject.FindGameObjectWithTag ("Digit4").GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Alpha0))
		{
			input = input + "0";
			Debug.Log ("0 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}	
		else if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{
			input = input + "1";	
			Debug.Log ("1 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			input = input + "2";
			Debug.Log ("2 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha3)) 
		{
			input = input + "3";
			Debug.Log ("3 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha4)) 
		{
			input = input + "4";
			Debug.Log ("4 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha5)) 
		{
			input = input + "5";
			Debug.Log ("5 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha6)) 
		{
			input = input + "6";
			Debug.Log ("6 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha7)) 
		{
			input = input + "7";
			Debug.Log ("7 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha8)) 
		{
			input = input + "8";
			Debug.Log ("8 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}
		else if (Input.GetKeyDown (KeyCode.Alpha9)) 
		{
			input = input + "9";
			Debug.Log ("9 is pressed");
			Debug.Log ("input is " + input);
			UpdateSprites ();
		}

		if (input.Length == 4) 
		{
			CheckIfCorrect ();
		}
	}

	public void UpdateSprites()
	{
		if (input.Length == 1) 
		{
			int tempDigit1 = System.Convert.ToInt32 (input [0] - 48);
			Debug.Log ("tempdigit1 is " + tempDigit1);
			digit1.sprite = spriteArray [tempDigit1];
		}
		if (input.Length == 2) 
		{
			int tempDigit2 = System.Convert.ToInt32 (input [1] -48);
			Debug.Log ("tempdigit2 is " + tempDigit2);
			digit2.sprite = spriteArray [tempDigit2];
		}
		if (input.Length == 3) 
		{
			int tempDigit3 = System.Convert.ToInt32 (input [2] -48);
			Debug.Log ("tempdigit3 is " + tempDigit3);
			digit3.sprite = spriteArray [tempDigit3];
		}
		if (input.Length == 4) 
		{
			int tempDigit4 = System.Convert.ToInt32 (input [3] -48);
			Debug.Log ("tempdigit4 is " + tempDigit4);
			digit4.sprite = spriteArray [tempDigit4];
		}

	}

	public void CheckIfCorrect() 
	{
		if (input == password) 
		{
			Debug.Log ("You're in!");
			backgroundColour.material.SetColor ("_Color", Color.green);
		}
		if (input != password) 
		{
			input = "";
			Debug.Log ("Try Again");
			digit1.sprite = spriteArray [10];
			digit2.sprite = spriteArray [10];
			digit3.sprite = spriteArray [10];
			digit4.sprite = spriteArray [10];

			StartCoroutine (Flash ());
		}
	}

	IEnumerator Flash()
	{
		backgroundColour.material.SetColor ("_Color", Color.red);
		yield return new WaitForSeconds (.5f);
		backgroundColour.material.SetColor ("_Color", Color.white);
		yield return new WaitForSeconds (.5f);
		backgroundColour.material.SetColor ("_Color", Color.red);
		yield return new WaitForSeconds (.5f);
		backgroundColour.material.SetColor ("_Color", Color.white);

	}
}